from src import app
import json
from flask import request
from transformers import pipeline, AutoTokenizer, AutoModelForTokenClassification
import numpy as np

class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)

sentiment_pipeline = pipeline("sentiment-analysis",model="siebert/sentiment-roberta-large-english")
fake_news_pipeline = pipeline("text-classification", model="XSY/albert-base-v2-fakenews-discriminator")
entity_tokenizer = AutoTokenizer.from_pretrained("dslim/bert-base-NER")
entity_model = AutoModelForTokenClassification.from_pretrained("dslim/bert-base-NER")
entity_nlp = pipeline("ner", model=entity_model, tokenizer=entity_tokenizer)
multilang_pipeline = pipeline("text-classification", "philschmid/distilbert-base-multilingual-cased-sentiment")

@app.route("/")
def hello():
    return json.dumps(sentiment_pipeline("I love this!"))

@app.route("/title_sentiment", methods = ["POST"])
def title_sentiment_analysis():
    return json.dumps(sentiment_pipeline(request.json['text']))

@app.route("/entity_sentiment", methods = ["POST"])
def entity_sentiment_analysis():
    return json.dumps(sentiment_pipeline(request.json['text']))

@app.route("/fake_news", methods = ["POST"])
def fake_news():
    return json.dumps(fake_news_pipeline(request.json['text']))

@app.route("/entity_extraction", methods = ["POST"])
def entity_extraction():
    return json.dumps(entity_nlp(request.json['text']), cls=NpEncoder)

@app.route("/multilingual_sentiment", methods = ["POST"])
def multilingual_sentiment():
    return json.dumps(multilang_pipeline(request.json['text']))